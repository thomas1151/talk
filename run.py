import os
from tornado.options import options, define, parse_command_line
import tornado.httpserver
import tornado.ioloop
import tornado.websocket
import tornado.wsgi
import tornado.web


os.environ['DJANGO_SETTINGS_MODULE'] = 'talk.settings' # path to your settings module

from django.core.wsgi import get_wsgi_application
import django.core.handlers.wsgi
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from chat.tasks import ChatHandler

def main():
    django.setup()
    define('port', type=int, default=8080)
    wsgi_app = tornado.wsgi.WSGIContainer(
        django.core.handlers.wsgi.WSGIHandler()
    )
    tornado_app = tornado.web.Application(
    [
      ('/cw', ChatHandler),
      ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
      ])
    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
