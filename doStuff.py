# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 19:58:40 2016

@author: Thomas
"""

import os
import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "start.settings")

django.setup()
# your imports, e.g. Django models
from chat.models import Question,Choice
from django.utils import timezone

q = Question.objects.get(pk=2)


# From now onwards start your script..