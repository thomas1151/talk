[13/08/2015 12:39:12] Harry: so what you been up to?
[13/08/2015 12:39:44] Aidan: well I just got back from wales on monday and then going down south again early saturday
[13/08/2015 12:40:09] Harry: was Wales 90% sheep?
[13/08/2015 12:40:51] Aidan: not really, we were on the coast so maybe there is a shortage there, we were outside of a town named borth though; basically the spennymoor of wales
[13/08/2015 12:41:04] Harry: borthbongo
[13/08/2015 12:41:15] Aidan: They had a carnival: It was aweful
[13/08/2015 12:41:35] Harry: omg they should have hired me to do sarcastic card tricks
[13/08/2015 12:41:44] Aidan: a worrying amount of men dressed as promiscious nurses and women
[13/08/2015 12:42:08] Harry: that may have been a gay pride parade
[13/08/2015 12:43:14] Aidan: no, just the " Borth carnival" I think the town jsut had a weird community
[13/08/2015 12:43:32] Aidan: How did today go then may I ask :D??
[13/08/2015 12:43:56] Harry: what do you mean?
[13/08/2015 12:45:06] Aidan: Your result??
[13/08/2015 12:45:37] Harry: I dunno, I didn't know they were out
[13/08/2015 12:45:48] Harry: i know my brother got his results
[13/08/2015 12:46:21] Aidan: Ah, well today, between 10am and 1pm was the opening for English Language results
[13/08/2015 12:46:42] Harry: meh i'll wait until they're all out
[13/08/2015 12:47:28] Aidan: I'm not sure what happens to the ones that are left; not sure if they'll go on the other list but I guess you'll find out!
[13/08/2015 12:47:42] Harry: yeah
[13/08/2015 12:47:48] Harry: so did you do alright?
[13/08/2015 12:48:37] Aidan: yeah; A8
[13/08/2015 12:48:45] Aidan: A*
[13/08/2015 12:48:49] Aidan: So very happy :)
[13/08/2015 12:48:57] Harry: good good :)
[13/08/2015 12:49:05] Harry: did you see anyone else there?
[13/08/2015 12:49:12] Harry: anyone having a breakdown?
[13/08/2015 12:49:51] Aidan: no, only a flock of 'gentlemen' (ben etc.) sat outside the 'auditorium'
[13/08/2015 12:50:18] Aidan: it made me wonder whether I should be waiting in a queue but apparently they just wanted to hang around?
[13/08/2015 12:50:26] Aidan: otherwise it was empty
[13/08/2015 12:51:03] Aidan: I imagin the rush would have been 10 am first thing
[13/08/2015 12:51:27] Harry: jesus i was still asleep then
[13/08/2015 12:51:33] Harry: that's like the middle of the night
[13/08/2015 12:51:39] Aidan: xD
[13/08/2015 12:52:20] Aidan: I awoke at 7am ready to take the day head on by doing nothing
[13/08/2015 12:53:01] Harry: god that sounds like hell