from django.shortcuts import get_object_or_404,render
from django.http import HttpResponseRedirect, HttpResponse,Http404,JsonResponse
from django.template.response import TemplateResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.views.generic import View
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.exceptions import ObjectDoesNotExist

import tornado.ioloop
import tornado.web

from tornado.options import define, options, parse_command_line

import socket
import datetime
from .models import Peer,Action,Chat
from .forms import ChatForm,NewChatForm
from .giver import start
#Using this type for index
class IndexView(View):
    @method_decorator(login_required(login_url='../chat/login'))
    def get(self,request,newChat = 0,):
        peer = request.user.id
        try:
            user = Peer.objects.get(pk=peer)
        except Peer.DoesNotExist:
            raise Http404("Database not configured correctly, no default user!")
        else:
            ##First check to see if new chat required
            try:
                newChat = int(request.GET.get('nC'))
            except TypeError:
                newChat = 0
            if newChat == 1:
                form = NewChatForm()
                server = socket.gethostbyname(socket.gethostname())
                return TemplateResponse(request,'chat/new_chat.html',{
                                                             'user': user,
                                                             'form' : form,
                                                             'server':server,
                                                                })



            #See if connected
            connected = request.GET.get('connected')
            print(connected)

            #If not connected
            if connected == None:
                form = NewChatForm()
                server = socket.gethostbyname(socket.gethostname())
                return TemplateResponse(request,'chat/index.html',{
                                                             'user': user,
                                                             'form' : form,
                                                             'server':server,
                                                             'chatData':0,
                                                                })
            #If connected
            if connected == "1":
                form = NewChatForm()
                server = socket.gethostbyname(socket.gethostname())
                return TemplateResponse(request,'chat/index.html',{
                                                             'user': user,
                                                             'form' : form,
                                                             'server':server,
                                                                })

            #If Wishing to review old chats.
            elif connected == '-1':
                chat = int(request.GET.get('chat')) #Chat ID To Load.
                try:
                    chat = Chat.objects.get(pk=chat)
                except ObjectDoesNotExist:
                    chat = 1
                actions = Action.objects.filter(chat_id=chat)
                form = ChatForm()
                otherData = {}
                otherData['peer1'] = Peer.objects.get(id=chat.user1_id)
                otherData['peer2'] = Peer.objects.get(id=chat.user2_id)

                #otherData['peer2'] = {}

                #otherData['peer1']['name'] = user.nick




                return TemplateResponse(request,'chat/index.html',{
                                                             'user': user,
                                                             'form' : form,
                                                             'actions': actions,
                                                             'chatData':chat,
                                                             'otherData':otherData,
                                                                })





    @csrf_protect
    def post(request):
        if request.method == 'POST':
            if request.POST['action'] == 'sendChat':
                action = request.POST.get('action')
                user = int(request.POST.get('sender')) #Add protection here
                #FIX THESE
                #User.objects.get(pk=sender)
                sender = Peer.objects.get(pk=user)
                chat = Chat.objects.get(pk=1)
                response_data = {}

                post = Action(text=action, sender=sender,chat=chat)
                post.save()

                response_data['result'] = 'Create post successful!'
                response_data['postpk'] = post.pk
                response_data['text'] = post.text
                response_data['senddate'] = post.senddate.strftime('[%d/%m/%y | %I:%M%p]')
                response_data['sender'] = post.sender.nick
                response_data['sender_intorext'] = post.sender.intorext
            elif request.POST['action'] == 'startChat':
                user1 = Peer.objects.get(pk=request.POST.get('user1')) #these need to b ids
                user2 = Peer.objects.get(pk=request.POST.get('user2')) #Peer connected to


                response_data = {}

                post = Chat(user1= user1,user2 =user2)
                response_data['result'] = 'New Chat Created.'
                response_data['user1'] = post.user1
                response_data['user2'] = post.user2
                return JsonResponse(response_data)

            return JsonResponse(response_data)
        else:

            return JsonResponse({"nothing to see": "this isn't happening"})



#Maybe provide info on specific action, not sure
class ActionView(generic.DetailView):
    model = Action
    template_name = 'chat/detail.html'

#Shows all data availble about USer, both this and above
#will help with learning about patterns etc.
class UserView(generic.DetailView):
    model = Peer
    template_name = 'chat/results.html'
