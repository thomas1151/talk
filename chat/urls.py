from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'chat'
urlpatterns = [
    url(r'^$',  views.IndexView.as_view(), name='index'),
    url(r'^post/$',  views.IndexView.post),
    url(r'^(?P<pk>[0-9]+)/$', views.ActionView.as_view(), name='action'),
    url(r'user/^(?P<pk>[0-9]+)/$', views.UserView.as_view(), name='user'),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
