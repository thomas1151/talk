# -*- coding: utf-8 -*-
"""
Created on Thu Mar 10 12:07:36 2016

@author: Thomas
"""

from django import forms

class ChatForm(forms.Form):
    action = forms.CharField(widget=forms.TextInput(attrs={'autocomplete':'off','id':'chat-form-text'}),label='Text', max_length=100)

class NewChatForm(forms.Form):
    peer = forms.CharField(widget=forms.TextInput(attrs={'autocomplete':'off','id':'new-chat-form-peer'}),label='Peer', max_length=100)
