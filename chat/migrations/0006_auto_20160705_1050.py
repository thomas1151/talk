# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0005_peer_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='chat',
            name='topic',
            field=models.CharField(max_length=14, default='Not analysed'),
        ),
        migrations.AddField(
            model_name='chat',
            name='topic2',
            field=models.CharField(max_length=14, default='Not analysed'),
        ),
    ]
