# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0003_action_chat'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='User',
            new_name='Peer',
        ),
        migrations.AlterModelOptions(
            name='action',
            options={'ordering': ['senddate']},
        ),
        migrations.AlterField(
            model_name='action',
            name='senddate',
            field=models.DateTimeField(auto_now_add=True, verbose_name='date sent'),
        ),
    ]
