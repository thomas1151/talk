from django.contrib import admin
from .models import Action,Peer,Chat
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User


# Register your models here.
admin.site.register(Action)
admin.site.register(Peer)
admin.site.register(Chat)


# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class PeerInLine(admin.StackedInline):
    model = Peer
    can_delete = False
    verbose_name_plural = 'peer'

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (PeerInLine, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)