
from tornado import ioloop
from tornado import httpclient

def handle_request(response):
    if response.error:
        return("Error: "+response.error)
    else:
        return(response.body)

def start():
    http_client = httpclient.AsyncHTTPClient()

    return(http_client.fetch("http://www.google.com/", handle_request).__dict__)
