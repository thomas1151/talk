from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Peer(models.Model):
    user = models.OneToOneField(User)
    #Binds Peer to User (Django User so logging in and out is supported)

    ip = models.CharField(max_length=50)
    #IP Address of last login

    nick = models.CharField(max_length=12)
    #Display Name

    intorext = models.IntegerField(default=0)
    #   type = int
    #   0 = Disconnected
    #   1 = Connected (Internal)
    #   2 = Connected (External)

    credate = models.DateTimeField("Date Created")
    #   type: datetime
    #   Date of Creation

    age = models.IntegerField()
    #   type = int
    #   Age of user

    def __str__(self):
        '''
        Returns the Nickname if model is requested.
        '''
        return self.nick

class Chat(models.Model):
    date = models.DateTimeField("Date Started")
    user1 = models.ForeignKey(Peer,related_name="peer1")
    user2 = models.ForeignKey(Peer,related_name="peer2")
    topic = models.CharField(max_length=14,default="Not analysed")
    topic2 = models.CharField(max_length=14,default="Not analysed")


class Action(models.Model):
    text = models.CharField(max_length=400)

    senddate = models.DateTimeField('date sent',auto_now_add=True)
    sender = models.ForeignKey(Peer)
    chat = models.ForeignKey(Chat)
    def __str__(self):
        return self.text

    class Meta:
        ordering = ['senddate']
