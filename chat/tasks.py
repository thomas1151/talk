from celery import Task
from celery import shared_task
import time
import socket
import json
from .models import Peer,Action,Chat

from tornado.options import options, define, parse_command_line
import tornado.httpserver
import tornado.ioloop
import tornado.websocket
import tornado.wsgi
import tornado.web

from .models import Peer,Action,Chat


class ChatHandler(tornado.websocket.WebSocketHandler):
    # Simple Websocket echo handler. This could be extended to
    # use Redis PubSub to broadcast updates to clients.

    clients = set()
    def check_origin(self, origin):
        return True


    def open(self):
        # logging.info('Client connected')
        ChatHandler.clients.add(self)
        #print(self.__dict__)

    def search():
        i = 0
        peers = Peer.objects.all()
        data = {}
        for p in peers:
            data[p.nick] = {'id':p.id,'ip':p.ip}
        ChatHandler.broadcast(json.dumps(data))

    def on_message(self, message):
        # logging.log('Received message')
             #If ready
        if message == "0":
            ##print(message)
            ChatHandler.search()

    def on_close(self):
        # logging.info('Client disconnected')
        if self in ChatHandler.clients:
            ChatHandler.clients.remove(self)

    @classmethod
    def broadcast(cls, message):
        for client in cls.clients:
            client.write_message(message)






class Getter(Task):
    """Client.

    Creates the client object
    Parameters
    ----------
    None

    Returns
    -------
    None


    """
    def __init__(self):
        ignore_result = True
        """Client init.

        Parameters
        ----------
        parent | parent obj
        port   | to run service on

        Returns
        -------
        None


        """
        self.s = socket.socket()        #Create a socket object

        self.host = "127.0.0.1"               #Host i.p
        self.port = 317                #Reserve a port for your service
        self.status = True
        self.connected = False
        #l = open('recv.html','wb')
    def run(self):
        """Main loop of client.

        Parameters
        ----------
        None

        Returns
        -------
        None

        """
        while self.status == True:
             try:
                self.s.connect((self.host,self.port)) #connected == True

             except ConnectionRefusedError:
                time.sleep(5)


             except ConnectionResetError:
                time.sleep(5)
                self.ess("Connection Lost")

             except OSError:
                self.get(self.s)
             else:
                text = "Connected to "+str(socket.gethostbyaddr(self.host))
                #self.parent.write(text,"@C");
                self.get(self.s)

    def get(self,s):
        """Retreive data/action .

        Parameters
        ----------
        s | socket

        Returns
        -------
        None


        """
        while True:
            try:
                data = json.loads((s.recv(1024)).decode('utf-8'))
            except ValueError:
                return
            else:
                a =Action(data) #Rework
                self.emit(self.signal, a.do())

    def ess(self,info):
        return


class Giver(Task):
    """Server.

    Server Obj!
    Parameters
    ----------
    None

    Returns
    -------
    None


    """
    def __init__(self):
        """Init Server obj.

        Creates the server
        Parameters
        ----------
        parent | obj which owns it
        port   | port to open and run on

        Returns
        -------
        None


        """
        self.s = socket.socket() #open socket

        self.port = 316 # Reserve a port for service
        self.status = True
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connected = False
        #add to log
    def run(self):
        """Main loop of server.

        Parameters
        ----------
        None

        Returns
        -------
        None


        """
        while self.connected == False:
            try:
                self.c
                print("Trying to connect")
            except AttributeError:
                    try:
                        self.s.bind(('', self.port))

                    except OSError:
                        return ("Port is already in use")
                    else:
                        self.s.listen(5)
                        self.c,self.caddr = self.s.accept()
            except OSError:
                return
            else:
                self.connected = True
        while True:
            self.s.listen(5)
            self.c,self.caddr = self.s.accept()





    def send(self,sending):
        if len(sending) > 0:
            data = json.dumps(sending)
            self.c.send(data.encode('utf-8'))
        return
    connected = True

    #Used to senf information to the main thread.
    def ess(self,info):
        return #rework
