import os
import re
import datetime
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "talk.settings")
import django
import nltk
django.setup()
# your imports, e.g. Django models
from chat.models import Chat,Action,Peer
from django.core.exceptions import ObjectDoesNotExist
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import gensim
import pprint
from operator import itemgetter
#nltk.download()
posts = nltk.corpus.nps_chat.xml_posts()[:10000]


def dialogue_act_features(post):
     features = {}
     for word in nltk.word_tokenize(post):
         features['contains({})'.format(word.lower())] = True
     return features


# From now onwards start your script..
def process(f,usr1, usr2,chusr1,chusr2,date):
    c = Chat()
    c.date = date
    
    try:
        peer1 = Peer.objects.get(nick=chusr1)
        peer2 = Peer.objects.get(nick=chusr2)
    except Peer.DoesNotExist:
        print("Users under that name not available")
    else:  
        c.user1_id = peer1.id
        c.user2_id = peer2.id
        c.save()
        fi = open("data/"+f)
        for line in fi:

            print(line)
            
            line = re.split("\[(.*?)\]",line)
            content = line[2].split(':')
            if content[0] == usr1:
                peerid = peer1.id
            if content[0] == usr2:
                peerid = peer2.id
            
            print("Name:",content)
            a = Action(text=content[1],senddate=datetime.datetime.strptime(line[1], '%d/%m/%Y %H:%M:%S'),sender_id=peerid,chat_id=c.id)
            a.save()
        fi.close()
        

def tag_and_remove(stop_list,actions):
        texts = []
        #For each action
        for a in actions:
            #Lower the text so its lower case
            text = a.text.lower()
            
            #For each word in the stop list
            for line in stop_list:
                #Remove it
                text = text.replace(" "+line+" "," ")
                
            text = text.split('\n')[0]
            text = text.split()
            texts.append(text) #Tokenised now, each action is its own seperate list.
    
        #Stemmed now too
        p_stemmer = PorterStemmer()
        for text in texts:
            [p_stemmer.stem(l) for l in text]
        
        #Word Classes to be removed
        stop_word_types = ['RB','RBR','RBS','TO','UH','EX','IN','JJR','JJS','LS','POS','WDT','WRB','NN','MD']
        stop_word_types = ['']
        #Duplicate existing list
        unbagged = texts 
        texts = []
        #For each action in unbagged text
        for un in range(0,len(unbagged)-1):
            texts.append([])
            #Part of Speech tagging
            unbagged[un] = nltk.pos_tag(unbagged[un])
            print(unbagged[un])
            #For word in action
            for v in range(0,len(unbagged[un])-1):
                #print(unbagged[un][v])
                for swt in stop_word_types:
                    if swt == unbagged[un][v][1]:
                        swtf = 1
                        break
                    else:
                        swtf = 0 #stop word type !found
                if swtf == 0:
                    texts[un].append(unbagged[un][v][0])
        print(texts)
        print(len(texts)-1)
        for i,t in enumerate(texts):
            if len(t) == 0:
                texts.pop(i)
            
        #The text is now stopwordless and void of any words classes not necessary
        return texts
                
                
def analyse(chat_id,save_location="analysis/",mode=1):
    
    if mode == 0:
        if not os.path.exists(save_location):
            os.makedirs(save_location)
        #Open Stop words (words which we don't need for analysis)
        stop_words = open("data/analysis/stop_words.txt") 
        
        #List of stop words
        stop_list = []
        for line in stop_words:
            #Append each line of file to stop words list in RAM, removing the new line value
            stop_list.append((line.split('\n')[0]))
        stop_words.close()
        #Get the chat that needs to be analysed
        c = Chat.objects.get(id=chat_id)
        users = [Peer.objects.get(id=c.user1_id),Peer.objects.get(id=c.user2_id)]
        texts = []
        #Get the actions associated with chat that needs to be analysed
        actions = Action.objects.filter(chat_id=chat_id)
        

        texts = tag_and_remove(stop_list,actions,)
    
        #print(unbagged)
        print(texts)
        #Gensim corpora creation
        dictionary = corpora.Dictionary(texts)
        #Make Bags of words ready for Lda (Word ID, Freq)
        corpus = [dictionary.doc2bow(text) for text in texts]
    
        #print(corpus)
        try:
            ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=2, id2word = dictionary, passes=20)
        except ValueError:
            pass
        else:
            topics = ldamodel.print_topics(num_topics=3, num_words=2)
            print(topics)
            for t in range(0,len(topics)):
                topics[t] = topics[t][1].split(" + ")
            #Flatten and split to topic word and probability
            topics = [item.split("*") for sublist in topics for item in sublist]
            for i in range(0,len(topics)):
                if 
            print(topics)
#            c.topic = topics[0][1].title()+topics[0][1].title()
#            c.topic2 = topics[1][1].title()
#            c.save()
            for u in users:
                ldamodel.save(save_location+str(u.nick)+"-lda.model")
        
    
if __name__ == '__main__':
    #process("181015.txt"," Mark James"," Thomas Barratt","mark","thomas1151",'2015-10-18 16:39:55')
#    for t in range(0,3):    
#        for i in range(1,len(Chat.objects.all())):
#            try:
#                analyse(i,mode=0)
#            except ObjectDoesNotExist:
#                pass
                analyse(15,mode=0)

